﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {

	public static GameMaster instance { get; private set;}
	
    [SerializeField]
    private GameObject submarinePrefab;
    [SerializeField]
    private Color[] fogColors;
    [SerializeField]
    private Transform playerStart;

    #region Properties

    public GameObject submarine { get; private set; }
    public bool gameIsOngoing { get; private set; }
    public int score { get; set; }
    public CheckPointData lastCheckpoint { get; set; }
    public float endTime { get { return _endTime; } }

    //Values to determine if its been the first time a tutorial was shown 
    public bool firstTimeCol { get; set; }
    public bool firstTimeTorp { get; set; }
    public bool firstTimeLightFlare { get; set; }
    public bool firstTimePropeller { get; set; }

    #endregion

    #region Private members

    //private Vector3 lastCheckpoint; //last known checkpoint position of the submarine
    private Quaternion checkpointRotation; //rotation to begin with at the checkpoint
    private int lastScore;
    private int currentFogColor = 0;
    private string partnerName = "Ringo";
    private float startTime;
    private float _endTime;
	private bool isReloadLevel;

    #endregion

    #region Events

    public static event System.Action OnGameStarted;

    #endregion

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
            
        else
            Destroy(gameObject);
    }

    void Start()
    {
        lastCheckpoint = new CheckPointData(playerStart.position, playerStart.rotation);
        lastScore = 0;
        //Again, for tutorial purpose we want them to be reset only when a game is remade
        firstTimeCol = true;
        firstTimeLightFlare = true;
        firstTimeTorp = true;
        firstTimePropeller = true;
        if (!ApplicationManager.instance.isTestingSolo)
            networkView.RPC("ExchangeNames", RPCMode.Others, SystemInfo.deviceName);

		isReloadLevel = true;
    }

    void Update()
    {
        GamePadManager gamePad = GamePadManager.instance;
        gamePad.UpdateGamePad();

        if (Input.GetKeyDown(KeyCode.Escape) || gamePad.GetButtonDown(XboxButtons.START))
        {
            networkView.RPC("TogglePause", RPCMode.All);
        }
    }

    void OnDestroy()
    {
        Submarine.OnHealthDecreased -= new System.Action<float>(CheckLoseCondition);
    }

    private void CheckLoseCondition(float submarineHealth)
    {
        if (submarineHealth <= 0.0f)
            networkView.RPC("GameOver", RPCMode.All);
    }

    private IEnumerator DeathVibration()
    {
        Camera.main.GetComponent<CameraShake>().DoShake();
        yield return new WaitForSeconds(0.3f);
        while (GamePadManager.instance.vibrating)
            yield return null;
        Time.timeScale = 0.0f;
    }

    public void StartGame()
    {
		RenderSettings.fogColor = fogColors[0];
        RenderSettings.fog = true;
		Time.timeScale = 1.0f;
		AudioManager.instance.StartMusic();
        score = lastScore;
        gameIsOngoing = true;

        if (Network.isServer)
        {
            submarine = Network.Instantiate(submarinePrefab, lastCheckpoint.position, lastCheckpoint.rotation, 0) as GameObject;
			InGameUIManager.instance.damageBackground.SetActive(false);
            Submarine.OnHealthDecreased += new System.Action<float>(CheckLoseCondition);
        }
        else if (Network.isClient)
        {
            submarine = GameObject.FindGameObjectWithTag("Submarine");
        }
		submarine.collider.enabled = true;

		if (isReloadLevel)
		{
			isReloadLevel = false;
			startTime = Time.time;
		}

        OnGameStarted();
    }

	#region FOG
	public void increaseDarkness(bool fogOff){
		if(currentFogColor < fogColors.Length){
			currentFogColor++;
			StartCoroutine("darkenFogColor");
		}
		if (fogOff) {
			RenderSettings.fog = false;
		}
	}

	private IEnumerator darkenFogColor(){
		float elapsedTime = 0.0f;
		float totalTime = 5.0f;

		while (elapsedTime < totalTime)
		{
            if (currentFogColor -1 >= 0)
			    RenderSettings.fogColor = Color.Lerp(fogColors[currentFogColor-1], fogColors[currentFogColor], elapsedTime/totalTime);
			elapsedTime += Time.deltaTime;
			yield return null;
		}
	}

	public void decreaseDarkness(){
		if(currentFogColor > 0){
			currentFogColor--;
			StartCoroutine("ligthenFogColor");
		}
	}
	
	private IEnumerator ligthenFogColor(){
		float elapsedTime = 0.0f;
		float totalTime = 5.0f;
		
		while (elapsedTime < totalTime)
		{
			RenderSettings.fogColor = Color.Lerp(fogColors[currentFogColor+1], fogColors[currentFogColor], elapsedTime/totalTime);
			elapsedTime += Time.deltaTime;
			yield return null;
		}
	}

	public void decreaseFog(){
		StartCoroutine("reduceFogDensity");
	}
	private IEnumerator reduceFogDensity(){
		float elapsedTime = 0.0f;
		float totalTime = 10.0f;
		float initialDensity = RenderSettings.fogDensity;
		
		while (elapsedTime < totalTime)
		{
			RenderSettings.fogDensity = Mathf.Lerp(initialDensity, initialDensity-0.013f, elapsedTime/totalTime);
			elapsedTime += Time.deltaTime;
			yield return null;
		}
	}
	#endregion

	#region CHECKPOINTS AND MENUS
    [RPC]
	public void SaveScore()
    {
        lastScore = score;
        if (Network.isServer)
            networkView.RPC("SaveScore", RPCMode.Others);
    }
	
    [RPC]
	public void RestartFromLastCheckpoint()
    {
        if (Network.isClient)
        {
            networkView.RPC("RestartFromLastCheckpoint", RPCMode.Server);
            return;
        }   

		ApplicationManager.instance.LoadLevel(Application.loadedLevel);
	}

    [RPC]
    public void ReloadLevel()
    {
		isReloadLevel = true;

        if (Network.isClient)
        {
            networkView.RPC("ReloadLevel", RPCMode.Server);
            return;
        }  

        Transform ps = GameObject.Find("PlayerStart").transform;
        lastCheckpoint = new CheckPointData(ps.position, ps.rotation);
        lastScore = 0;
        networkView.RPC("ResetScore", RPCMode.Others);
        ApplicationManager.instance.LoadLevel(Application.loadedLevel);
    }

    [RPC]
    void ResetScore()
    {
        score = 0;
        lastScore = 0;
    }

	#endregion

    [RPC]
    private void GameOver()
    {
        submarine.collider.enabled = false;
        gameIsOngoing = false;
        InGameUIManager.instance.ShowGameOver();
        Camera.main.GetComponent<BlurEffect>().enabled = true;
        AudioManager.instance.StopMusic();
        StartCoroutine(DeathVibration());
    }

    [RPC]
    public void TogglePause()
    {
        gameIsOngoing = !gameIsOngoing;
        InGameUIManager.instance.TogglePauseMenu(!gameIsOngoing);
        Time.timeScale = gameIsOngoing ? 1.0f : 0.0f;
        GamePadManager.instance.Vibrate(0.0f, 0.0f, 0.0f);
    }

    [RPC]
    public void LevelCompleted()
    {
        gameIsOngoing = false;
        AudioManager.instance.StopMusic();
        Time.timeScale = 0.0f;

        LeaderboardManager.LoadScores();
        _endTime = Time.time - startTime;
        if (LeaderboardManager.AddScore(new Score(partnerName, score, _endTime)))
        {
            InGameUIManager.instance.ShowFinishedGame(true);
        }
        else
        {
            InGameUIManager.instance.ShowFinishedGame(false);
        }
        
    }

    [RPC]
    private void ExchangeNames(string name)
    {
        partnerName = name;
        if (Network.isClient)
            networkView.RPC("ExchangeNames", RPCMode.Server, SystemInfo.deviceName);
    }

}