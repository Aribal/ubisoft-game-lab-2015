﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public enum XboxButtons
{
    A, B, X, Y, RB, LB, UP, DOWN, RIGHT, LEFT, START, BACK
}
public enum XboxTriggers
{
    LEFT, RIGHT
}

public class GamePadManager : MonoBehaviour {

    public static GamePadManager instance { get; private set; }

    public GamePadState state
    {
        get { return _state; }
    }

    private bool _playerIndexSet = false;
    private PlayerIndex _playerIndex;
    private GamePadState _state;
    private GamePadState _prevState;
    private uint _vibrators = 0;
    private bool _wasUpdatedThisFrame = false;

    public bool vibrating { get; private set; }

	void Awake () {
        if (instance == null)
            instance = this;
        _state = GamePad.GetState(_playerIndex);
        vibrating = false;
	}

    void LateUpdate()
    {
        _wasUpdatedThisFrame = false;
    }

    // Must be called before any other methods of this class in an update loop
	public void UpdateGamePad()
    {
        if (_wasUpdatedThisFrame)
            return;
        else
            _wasUpdatedThisFrame = true;

        // Find a _playerIndex, for a single player game
        // Will find the first controller that is connected ans use it
        if (!_playerIndexSet || !_prevState.IsConnected)
        {
            for (int i = 0; i < 4; ++i)
            {
                PlayerIndex testPlayerIndex = (PlayerIndex)i;
                GamePadState testState = GamePad.GetState(testPlayerIndex);
                if (testState.IsConnected)
                {
                    Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
                    _playerIndex = testPlayerIndex;
                    _playerIndexSet = true;
                }
            }
        }

        _prevState = _state;
        _state = GamePad.GetState(_playerIndex);
    }

    private bool GetButtonDown(ButtonState prevBtnState, ButtonState btnState)
    {
        return (prevBtnState == ButtonState.Released && btnState == ButtonState.Pressed);
    }

    private bool GetButtonUp(ButtonState prevBtnState, ButtonState btnState)
    {
        return (prevBtnState == ButtonState.Pressed && btnState == ButtonState.Released);
    }

    private bool GetTriggerDown(float prevIntensity, float intensity, float threshold)
    {
        return (prevIntensity < threshold && intensity >= threshold);
    }

    private bool GetTriggerUp(float prevIntensity, float intensity, float threshold)
    {
        return (prevIntensity >= threshold && intensity < threshold);
    }

    private IEnumerator VibrateCoroutine(float leftMotor, float rightMotor, float duration)
    {
		vibrating = true;
        GamePad.SetVibration(_playerIndex, leftMotor, rightMotor);
		
        yield return new WaitForSeconds(duration);

        --_vibrators;

        if (_vibrators == 0) {
			GamePad.SetVibration (_playerIndex, 0.0f, 0.0f);
			vibrating = false;
		}
    }

    public bool GetButtonDown(XboxButtons button)
    {
        bool pressed = false;
        switch (button)
        {
            case XboxButtons.A:
                pressed = GetButtonDown(_prevState.Buttons.A, _state.Buttons.A);
                break;
            case XboxButtons.B:                              
                pressed = GetButtonDown(_prevState.Buttons.B, _state.Buttons.B);
                break;
            case XboxButtons.X:
                pressed = GetButtonDown(_prevState.Buttons.X, _state.Buttons.X);
                break;
            case XboxButtons.Y:
                pressed = GetButtonDown(_prevState.Buttons.Y, _state.Buttons.Y);
                break;
            case XboxButtons.RB:
                pressed = GetButtonDown(_prevState.Buttons.RightShoulder, _state.Buttons.RightShoulder);
                break;
            case XboxButtons.LB:
                pressed = GetButtonDown(_prevState.Buttons.LeftShoulder, _state.Buttons.LeftShoulder);
                break;
            case XboxButtons.UP:
                pressed = GetButtonDown(_prevState.DPad.Up, _state.DPad.Up);
                break;
            case XboxButtons.DOWN:
                pressed = GetButtonDown(_prevState.DPad.Down, _state.DPad.Down);
                break;
            case XboxButtons.RIGHT:
                pressed = GetButtonDown(_prevState.DPad.Right, _state.DPad.Right);
                break;
            case XboxButtons.LEFT:
                pressed = GetButtonDown(_prevState.DPad.Left, _state.DPad.Left);
                break;
            case XboxButtons.START:
                pressed = GetButtonDown(_prevState.Buttons.Start, _state.Buttons.Start);
                break;
            case XboxButtons.BACK:
                pressed = GetButtonDown(_prevState.Buttons.Back, _state.Buttons.Back);
                break;
            default:
                break;
        }

        return pressed;
    }

    public bool GetButtonUp(XboxButtons button)
    {
        bool released = false;
        switch (button)
        {
            case XboxButtons.A:
                released = GetButtonUp(_prevState.Buttons.A, _state.Buttons.A);
                break;
            case XboxButtons.B:
                released = GetButtonUp(_prevState.Buttons.B, _state.Buttons.B);
                break;
            case XboxButtons.X:
                released = GetButtonUp(_prevState.Buttons.X, _state.Buttons.X);
                break;
            case XboxButtons.Y:
                released = GetButtonUp(_prevState.Buttons.Y, _state.Buttons.Y);
                break;
            case XboxButtons.RB:
                released = GetButtonUp(_prevState.Buttons.RightShoulder, _state.Buttons.RightShoulder);
                break;
            case XboxButtons.LB:
                released = GetButtonUp(_prevState.Buttons.LeftShoulder, _state.Buttons.LeftShoulder);
                break;
            case XboxButtons.UP:
                released = GetButtonUp(_prevState.DPad.Up, _state.DPad.Up);
                break;
            case XboxButtons.DOWN:
                released = GetButtonUp(_prevState.DPad.Down, _state.DPad.Down);
                break;
            case XboxButtons.RIGHT:
                released = GetButtonUp(_prevState.DPad.Right, _state.DPad.Right);
                break;
            case XboxButtons.LEFT:
                released = GetButtonUp(_prevState.DPad.Left, _state.DPad.Left);
                break;
            case XboxButtons.START:
                released = GetButtonUp(_prevState.Buttons.Start, _state.Buttons.Start);
                break;
            case XboxButtons.BACK:
                released = GetButtonUp(_prevState.Buttons.Back, _state.Buttons.Back);
                break;
            default:
                break;
        }

        return released;
    }

    public bool GetTriggerDown(XboxTriggers trigger, float threshold = 0.1f)
    {
        bool pressed = false;
        if (trigger == XboxTriggers.LEFT)
            pressed = GetTriggerDown(_prevState.Triggers.Left, _state.Triggers.Left, threshold);
        else
            pressed = GetTriggerDown(_prevState.Triggers.Right, _state.Triggers.Right, threshold);
        return pressed;
    }

    public bool GetTriggerUp(XboxTriggers trigger, float threshold = 0.1f)
    {
        bool released = false;
        if (trigger == XboxTriggers.LEFT)
            released = GetTriggerUp(_prevState.Triggers.Left, _state.Triggers.Left, threshold);
        else
            released = GetTriggerUp(_prevState.Triggers.Right, _state.Triggers.Right, threshold);
        return released;
    }

    public void Vibrate(float leftMotor, float rightMotor, float duration = 1.0f)
    {
        ++_vibrators;
        StartCoroutine(VibrateCoroutine(leftMotor, rightMotor, duration));
    }
}
