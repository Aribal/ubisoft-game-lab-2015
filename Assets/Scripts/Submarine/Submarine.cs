﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Submarine : MonoBehaviour
{

    #region Inspector variables

    [SerializeField] private float maxSpeed;
    [SerializeField] private float normalSpeed = 3.0f;
    [SerializeField] private float acceleratedSpeed = 5.0f;
    [SerializeField] private float doubleAcceleratedSpeed = 7.0f;
	
	[SerializeField] private float stabilizationSpeed; //speed at which the submarine stabilizes
	[SerializeField] private float rotationSpeed; //speed at which the submarine rotates

	//particle systems for propellers
	[SerializeField] internal ParticleSystem particles;
	[SerializeField] internal ParticleSystem upParticles;
	[SerializeField] internal ParticleSystem bottomParticles;
	[SerializeField] internal ParticleSystem leftParticles;
	[SerializeField] internal ParticleSystem rightParticles;

	//Modules
    [SerializeField] private Module[] playerOneModules;
    [SerializeField] private Module[] playerTwoModules;
    [SerializeField] private string[] moduleNames;

	//Orientation locks
	[SerializeField] private float rotationAngleX;
	[SerializeField] private float rotationAngleY;
	[SerializeField] private float modelRotationAngle;

    public bool[] enabledModules;



    #endregion

    #region Private members

    private float targetSpeed;

    private Module currentPlayerOneModule;
    private Module currentPlayerTwoModule;
	private bool nullifingPropellers;

	private Vector3 truePosition;
	private Quaternion trueRotation;

	private Color lerpedColor;

	private Vector3 initialRotation; //Clamped by rotationAngleX and rotationAngleY
	private List<Transform> modelsRef; //References to submarine 3D models
	private Vector3[] modelsInitialRotations; //Used for rotation effect
	private Quaternion[] modelsActualRotations; //Used for rotation effect
	private float prevMoveHorizontal;
	private float prevMoveVertical;
	private Vector3 forceToAdd;
    internal float moveHorizontal;
    internal float moveVertical;
    private float myHealth;

    #endregion

    #region Properties

    public static Submarine current { get; private set; }

    public float health
    {
        get { return myHealth; }
        set
        {
            if (value > myHealth)
                OnHealthIncreased(value);
            else if (value < myHealth)
                OnHealthDecreased(value);
            myHealth = Mathf.Clamp(value, 0.0f, 100.0f);
            if (Network.isServer)
                networkView.RPC("UpdateClientHealth", RPCMode.Others, myHealth);
        }
    }
	public float submarineMass{ get { return rigidbody.mass; } }
    public float currentSpeed { get; private set; }
	public float currentMaxSpeed{ get { return maxSpeed; } }
    public static string currentModuleName { get; private set; }
    public bool isNearSurface { get; set; }

    #endregion

    #region Events

    public static event System.Action<float> OnHealthIncreased;
    public static event System.Action<float> OnHealthDecreased;

    #endregion

    /**wind variables*/
	public bool isInWindZone=false;
	public Vector3 windForce;

    #region MonoBehaviour messages

    void Start () {
        current = this;
		initialRotation = transform.eulerAngles;
        myHealth = 0.0f;
		health = 100.0f;
        currentSpeed = normalSpeed;
        targetSpeed = normalSpeed;
        isNearSurface = false;
		particles.startSpeed = normalSpeed*2;

		#region initialisation icones
		if (enabledModules [0])
			InGameUIManager.instance.A.enabled = true;
		else
			InGameUIManager.instance.A.enabled = false;
        if (enabledModules[1])
			InGameUIManager.instance.B.enabled = true;
		else{
			InGameUIManager.instance.B_No_Module();
		}
        if (enabledModules[2])
			InGameUIManager.instance.Y.enabled = true;
		else
			InGameUIManager.instance.Y_No_Module();
        if (enabledModules[3])
			InGameUIManager.instance.X.enabled = true;
		else
			InGameUIManager.instance.X_No_Module();
		#endregion

		//Initialize models references for models rotations
		modelsRef = new List<Transform>();
		modelsInitialRotations = new Vector3[transform.childCount];
		modelsActualRotations = new Quaternion[transform.childCount];

		int childCount = 0;
		foreach (Transform child in transform) {
			if (child.tag != "MainCamera") {
				modelsRef.Add(child);
				modelsInitialRotations[childCount] = child.localEulerAngles;
				modelsActualRotations[childCount] = child.rotation;
				childCount++;
			}
		}
		prevMoveHorizontal = 0;
		prevMoveVertical = 0;


		//Modules initialization
        currentPlayerOneModule = playerOneModules[0];
        currentPlayerTwoModule = playerTwoModules[0];
        currentModuleName = moduleNames[0];
		nullifingPropellers = false;

        if (enabledModules.Length != playerOneModules.Length){
            enabledModules = new bool[playerOneModules.Length];
            for (int i = 0; i < playerOneModules.Length; i++)
                enabledModules[i] = true;
        }
	}

	void FixedUpdate(){
		forceToAdd = -Physics.gravity * rigidbody.mass; //the submarine isn't affected by gravity

		if (isInWindZone) {
			rigidbody.AddForce(windForce);
		}

		rigidbody.AddForce (forceToAdd);

		UpdateSubmarineMovements();
	}

	void Update () {
        PollInput();

		if (nullifingPropellers)
			NullifyPropellers();

	}

    //Position streaming from server to client
    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        if (stream.isWriting)
        {
            //Serialize submarine rotation
            truePosition = transform.position;
            stream.Serialize(ref truePosition);

            trueRotation = transform.rotation;
            stream.Serialize(ref trueRotation);
        }
        else
        {
            //Unserialize submarine rotation
            stream.Serialize(ref truePosition);
            stream.Serialize(ref trueRotation);
        }
    }

    #endregion

    #region Private methods

    void PollInput()
    {
#if UNITY_EDITOR
        if (!ApplicationManager.instance.isFocused)
            return;
#endif

        if (!GameMaster.instance.gameIsOngoing)
            return;

        #region GAMEPAD
        GamePadManager gamePad = GamePadManager.instance;
        gamePad.UpdateGamePad();

        if (Network.isServer)
			currentPlayerOneModule.SendAxis(Input.GetAxis("Horizontal"), 
			                                ApplicationManager.instance.invertedAim ? -1.0f * Input.GetAxis("Vertical") : Input.GetAxis("Vertical"));
        else
            currentPlayerTwoModule.SendAxis(Input.GetAxis("Horizontal"),
			                                ApplicationManager.instance.invertedAim ? -1.0f * Input.GetAxis("Vertical") : Input.GetAxis("Vertical"));


        // Module usage

        if (GamePadManager.instance.GetTriggerDown(XboxTriggers.RIGHT))
        {
            UseModule();
        }

        if (GamePadManager.instance.GetTriggerUp(XboxTriggers.RIGHT))
        {
            StopModuleUsage();
        }

        // Module selection

        if (GamePadManager.instance.GetButtonDown(XboxButtons.A) && enabledModules[0])
        {
            SelectModule(0); // Propeller
			DisableModulesButtonsExcept(0);
			InGameUIManager.instance.A_ON();
			playModuleSelectionSound(0);
        }
        else if (GamePadManager.instance.GetButtonDown(XboxButtons.B) && enabledModules[1])
        {
            SelectModule(1);  // Torpedo
			DisableModulesButtonsExcept(1);
			InGameUIManager.instance.B_ON();
			playModuleSelectionSound(1);
        }
        else if (GamePadManager.instance.GetButtonDown(XboxButtons.X) && enabledModules[3])
        {
            SelectModule(3); // Repair
			DisableModulesButtonsExcept(3);
			InGameUIManager.instance.X_ON();
			playModuleSelectionSound(3);
        }
        else if (GamePadManager.instance.GetButtonDown(XboxButtons.Y) && enabledModules[2])
        {
            SelectModule(2); // Light || Flare
			DisableModulesButtonsExcept(2);
			InGameUIManager.instance.Y_ON();
			playModuleSelectionSound(2);
        }
		
        #endregion

        #region KEYBOARD CONTROLS

        if (Input.GetButtonDown("Fire1"))
            UseModule();
        if (Input.GetButtonUp("Fire1"))
            StopModuleUsage();

		if (Input.GetKeyDown(KeyCode.Alpha1) && enabledModules[0])
        {
            SelectModule(0);
            InGameUIManager.instance.A_ON();
			playModuleSelectionSound(0);

			//If modules have been unlocked or not
			if(enabledModules[3] == true)
				InGameUIManager.instance.X_OFF();
			else
				InGameUIManager.instance.X_No_Module();
			if(enabledModules[1] == true)
				InGameUIManager.instance.B_OFF();
			else
				InGameUIManager.instance.B_No_Module();
			if(enabledModules[2] == true)
            	InGameUIManager.instance.Y_OFF();
			else
				InGameUIManager.instance.Y_No_Module();
        }
		else if (Input.GetKeyDown(KeyCode.Alpha2) && enabledModules[1])
        {
            SelectModule(1);
            InGameUIManager.instance.A_OFF();
			playModuleSelectionSound(1);

			//If modules have been unlocked or not
			if(enabledModules[3] == true)
				InGameUIManager.instance.X_OFF();
			else
				InGameUIManager.instance.X_No_Module();
			if(enabledModules[1] == true)
				InGameUIManager.instance.B_ON();
			else
				InGameUIManager.instance.B_No_Module();
			if(enabledModules[2] == true)
				InGameUIManager.instance.Y_OFF();
			else
				InGameUIManager.instance.Y_No_Module();
        }
		else if (Input.GetKeyDown(KeyCode.Alpha3) && enabledModules[2])
        {
            SelectModule(2);
            InGameUIManager.instance.A_OFF();
			playModuleSelectionSound(2);

			//If modules have been unlocked or not
			if(enabledModules[3] == true)
				InGameUIManager.instance.X_OFF();
			else
				InGameUIManager.instance.X_No_Module();
			if(enabledModules[1] == true)
				InGameUIManager.instance.B_OFF();
			else
				InGameUIManager.instance.B_No_Module();
			if(enabledModules[2] == true)
				InGameUIManager.instance.Y_ON();
			else
				InGameUIManager.instance.Y_No_Module();
        }
		else if (Input.GetKeyDown(KeyCode.Alpha4) && enabledModules[3])
        {
            SelectModule(3);
            InGameUIManager.instance.A_OFF();
			playModuleSelectionSound(3);

			//If modules have been unlocked or not
			if(enabledModules[3] == true)
				InGameUIManager.instance.X_ON();
			else
				InGameUIManager.instance.X_No_Module();
			if(enabledModules[1] == true)
				InGameUIManager.instance.B_OFF();
			else
				InGameUIManager.instance.B_No_Module();
			if(enabledModules[2] == true)
				InGameUIManager.instance.Y_OFF();
			else
				InGameUIManager.instance.Y_No_Module();
        }

        #endregion
    }

	private void DisableModulesButtonsExcept(int moduleNumber){

		if(enabledModules[0] && moduleNumber!=0)
			InGameUIManager.instance.A_OFF();
		if(enabledModules[1] && moduleNumber!=1)
			InGameUIManager.instance.B_OFF();
		else
			InGameUIManager.instance.B_No_Module();
		if(enabledModules[3] && moduleNumber!=3)
			InGameUIManager.instance.X_OFF();
		else
			InGameUIManager.instance.X_No_Module();
		if(enabledModules[2] && moduleNumber!=2)
			InGameUIManager.instance.Y_OFF();
		else
			InGameUIManager.instance.Y_No_Module();
	}

	private void playModuleSelectionSound(int moduleNumber){
		switch(moduleNumber){
		case 0:
			if(Network.isServer)
				AudioManager.instance.playLateralPropellersSelected();
			else
				AudioManager.instance.playVerticalPropellersSelected();
			return;
		case 1:
			AudioManager.instance.playTorpedoesActivated();
			return;
		case 2:
			if(Network.isServer)
				AudioManager.instance.playLightActivated();
			else
				AudioManager.instance.playFlareActivated(); 
			return;
		case 3:
			AudioManager.instance.playRepairActivated();
			return;
		}
	}

    void UpdateSubmarineMovements() {
		Vector3 eulerAngleVelocity = new Vector3(moveVertical, moveHorizontal, 0.0f);
		Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime * 50.0f);
        
        if (networkView.isMine) {
            //Server moves submarine forward
            currentSpeed = Mathf.Lerp(currentSpeed, targetSpeed, Time.deltaTime * 5.0f);
            transform.Translate(Vector3.forward * Time.deltaTime * currentSpeed);
            if (isNearSurface && transform.eulerAngles.x < 0)
                transform.position = new Vector3(transform.position.x,
                    WaterSurface.currentSurfaceLevel,
                    transform.position.z);
		}
        else
        {
			//Client corrects its positions
			transform.position = Vector3.Lerp(transform.position, this.truePosition, Time.deltaTime * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, this.trueRotation, Time.deltaTime * 5);
		}

		//Submarine rotates. Client updates itself in prediction of the true position
		if(Network.isServer)
        {
			transform.Rotate(Vector3.up, rotationSpeed * moveHorizontal * Time.deltaTime, Space.World);
			transform.Rotate(Vector3.right, rotationSpeed * moveVertical * Time.deltaTime, Space.Self);
		}


		//Stabilize Z axis
		if (transform.rotation.z != 0.0f)
        {
			if (transform.eulerAngles.z > 180.0f && transform.eulerAngles.z <= 359.5f)
            {
				transform.Rotate (Vector3.forward, deltaRotation.z+stabilizationSpeed);
			}
			else if (transform.eulerAngles.z > 0.5f && transform.eulerAngles.z <= 180.0f)
            {
				transform.Rotate (Vector3.forward, deltaRotation.z-stabilizationSpeed);
			}

		}

		//Lock X and Y axis
		Vector3 clampedRotation = transform.eulerAngles;

		//Lock X
		if(360-clampedRotation.x > 180){
			clampedRotation.x = Mathf.Clamp(clampedRotation.x, 0, initialRotation.x+rotationAngleX);
		}
		else if(360-clampedRotation.x < 180){
			clampedRotation.x = Mathf.Clamp(clampedRotation.x, 360+(initialRotation.x-rotationAngleX), 360);
		}
		//Lock Y
		clampedRotation.y = Mathf.Clamp(clampedRotation.y, initialRotation.y-rotationAngleY, initialRotation.y+rotationAngleY);
		//Lock Z
		clampedRotation.z = Mathf.Clamp(clampedRotation.z, 0, 0);

		//Apply lock
		transform.rotation = Quaternion.Euler(clampedRotation);

		//Rotate models effect. Client prediction
		int modelCount = 0;
		foreach (Transform model in modelsRef){
			float verticalRotToApply = 0;
			float horizontalRotToApply = 0;

			if (Network.isServer && currentPlayerOneModule == playerOneModules[0]
                || Network.isClient && currentPlayerTwoModule == playerTwoModules[0])
            {
				verticalRotToApply = moveVertical;
				horizontalRotToApply = moveHorizontal;
			}

			//If horizontal movement changes direction, dampen rotation change
			if(prevMoveHorizontal<0 && moveHorizontal>=0 || prevMoveHorizontal>0 && moveHorizontal<=0){
				NullifyHorizProp(ref prevMoveHorizontal);

				horizontalRotToApply = prevMoveHorizontal;
			}
			else
            {//Regular rotations
				prevMoveHorizontal = moveHorizontal;
			}

			//If vertical movement changes direction, dampen rotation change
			if(prevMoveVertical<0 && moveVertical>=0 || prevMoveVertical>0 && moveVertical<=0){
				NullifyVertProp(ref prevMoveVertical);
				
				verticalRotToApply = prevMoveVertical;
			}
			else{//Regular rotations
				prevMoveVertical = moveVertical;
			}

			float rotationToApplyX = modelsInitialRotations[modelCount].x + verticalRotToApply * modelRotationAngle;
			float rotationToApplyY = modelsInitialRotations[modelCount].y + horizontalRotToApply * modelRotationAngle;

			if (networkView.isMine) {
				modelsActualRotations[modelCount] = Quaternion.Euler(rotationToApplyX, rotationToApplyY, model.localEulerAngles.z);
			}

			//Apply rotation
			model.localRotation = Quaternion.RotateTowards(model.localRotation, Quaternion.Euler(rotationToApplyX, rotationToApplyY, model.localEulerAngles.z), 75*Time.deltaTime);
			modelCount++;
		}
    }

	//Reset player rotations
	void NullifyPropellers(){
		NullifyVertProp(ref moveVertical);
		NullifyHorizProp(ref moveHorizontal);

		if(moveVertical == 0 && moveHorizontal == 0)
			nullifingPropellers = false;
	}
	void NullifyVertProp(ref float axis){
		if(axis  >= 0.07f)
			axis -= 0.07f;
		else if(axis < -0.07f)
			axis += 0.07f;
		else if (axis != 0f)
			axis = 0f;
	}
	void NullifyHorizProp(ref float axis){
		if(axis >= 0.07f)
			axis -= 0.07f;
		else if(axis < -0.07f)
			axis += 0.07f;
		else if (axis != 0f)
			axis = 0f;
	}
	
    private void SelectModule(int index)
    {
		networkView.RPC("NullifyPropellersCall", RPCMode.All); //If we change the modules, make sure orientation change is halted.
        
		if (Network.isServer)
            networkView.RPC("ReplicatePlayerOneModuleChange", RPCMode.All, index);
        else
            networkView.RPC("ReplicatePlayerTwoModuleChange", RPCMode.All, index);
    }

    private void UseModule()
    {
		//Sounds
		if (Network.isServer && currentPlayerOneModule.GetType() == 0 || !Network.isServer && currentPlayerTwoModule.GetType() == 0)
			playModuleUsageSound(0);
		else if (Network.isServer && currentPlayerOneModule.GetType() == 1 || !Network.isServer && currentPlayerTwoModule.GetType() == 1)
			playModuleUsageSound(1);
		else if (Network.isServer && currentPlayerOneModule.GetType() == 3 || !Network.isServer && currentPlayerTwoModule.GetType() == 3)
			playModuleUsageSound(3);
		else if (Network.isServer && currentPlayerOneModule.GetType() == 2 || !Network.isServer && currentPlayerTwoModule.GetType() == 2)
			playModuleUsageSound(2);

		//Usage
		if (Network.isServer)
            networkView.RPC("ReplicatePlayerOneModuleUsage", RPCMode.All);
        else
            networkView.RPC("RequestModuleUsage", RPCMode.Server);
    }

    private void StopModuleUsage()
    {		
		if (Network.isServer)
            networkView.RPC("ReplicatePlayerOneModuleUsageStop", RPCMode.All);
        else
            networkView.RPC("RequestModuleUsageStop", RPCMode.Server);
    }

	private void playModuleUsageSound(int moduleNumber){
		switch(moduleNumber){
		case 0:
			AudioManager.instance.playPropellersUsed();
			return;
		case 1:
			AudioManager.instance.playTorpedoesUsed();
			return;
		case 2:
			if(Network.isServer)
				AudioManager.instance.playLightUsed();
			else
				AudioManager.instance.playFlareUsed(); 
			return;
		case 3:
			AudioManager.instance.playRepairUsed();
			return;
		}
	}

    #endregion

    #region Public methods

    public void MoveHorizontal(float val)
    {
        moveHorizontal = val;
    }

    public void MoveVertical(float val)
    {
        moveVertical = val;
    }

    public void Accelerate()
    {
        if (targetSpeed == normalSpeed) {
			targetSpeed = acceleratedSpeed;
			particles.startSpeed = acceleratedSpeed*2;
		} else if (targetSpeed == acceleratedSpeed) {
			targetSpeed = doubleAcceleratedSpeed;
			particles.startSpeed = doubleAcceleratedSpeed*2;
		}
    }

    public void Decelerate()
    {
        if (targetSpeed == acceleratedSpeed) {
			targetSpeed = normalSpeed;
			particles.startSpeed = normalSpeed*2;
		} else if (targetSpeed == doubleAcceleratedSpeed) {
			targetSpeed = acceleratedSpeed;
			particles.startSpeed = acceleratedSpeed*2;
		}
    }

    #endregion

    #region RPCs

    //Stop movement input when a player changes module
	[RPC] void NullifyPropellersCall()
    {
		nullifingPropellers = true;
	}

	//Server movement input
	[RPC] void horizontalMovement(float input)
	{
		moveHorizontal = input;
	}
	
	//Client movement input
	[RPC] void verticalMovement(float input)
    {
		moveVertical = input;

	}

    [RPC] void ReplicatePlayerOneModuleChange(int index)
    {
        if (!enabledModules[index] || currentPlayerOneModule == playerOneModules[index])
            return;

        currentPlayerOneModule.OnUnselected();

        currentPlayerOneModule = playerOneModules[index];
        currentModuleName = moduleNames[index];

        currentPlayerOneModule.OnSelected();
    }

    [RPC] void ReplicatePlayerTwoModuleChange(int index)
    {
        if (!enabledModules[index] || currentPlayerTwoModule == playerTwoModules[index])
            return;

        currentPlayerTwoModule.OnUnselected();

        currentPlayerTwoModule = playerTwoModules[index];
        currentModuleName = moduleNames[index];

        currentPlayerTwoModule.OnSelected();
    }

    [RPC] void RequestModuleUsage()
    {
        networkView.RPC("ReplicatePlayerTwoModuleUsage", RPCMode.All);
    }

    [RPC] void ReplicatePlayerOneModuleUsage()
    {
        currentPlayerOneModule.Use();
    }

    [RPC] void ReplicatePlayerTwoModuleUsage()
    {
        currentPlayerTwoModule.Use();
    }

    [RPC] void RequestModuleUsageStop()
    {
        networkView.RPC("ReplicatePlayerTwoModuleUsageStop", RPCMode.All);
    }

    [RPC] void ReplicatePlayerOneModuleUsageStop()
    {
        currentPlayerOneModule.StopUse();
    }

    [RPC] void ReplicatePlayerTwoModuleUsageStop()
    {
        currentPlayerTwoModule.StopUse();
    }

    [RPC]
    void UpdateClientHealth(float newValue)
    {
        health = newValue;
    }

    [RPC]
    public void UpdateModules(int bits)
    {
		int modNumber = 0;
        // If the bit corresponding to the module is set to 1, enable it
        enabledModules[0] = (bits & (1 << 0)) != 0;
        enabledModules[1] = (bits & (1 << 1)) != 0;
        enabledModules[2] = (bits & (1 << 2)) != 0;
        enabledModules[3] = (bits & (1 << 3)) != 0;
		if (currentModuleName == "Propeller")
			modNumber = 0;
		if (currentModuleName == "Torpedo"){
			modNumber = 1;
		if (currentModuleName == "Repair")
			modNumber = 3;
		if (currentModuleName == "Light" || currentModuleName == "Flare")
			modNumber = 2;
		}
		DisableModulesButtonsExcept (modNumber);

    }

    #endregion
}
