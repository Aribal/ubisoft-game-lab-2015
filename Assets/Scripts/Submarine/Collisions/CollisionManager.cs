﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CollisionManager : MonoBehaviour {

    [SerializeField]
    private float collisionDamage; //damage taken for each collision
	[SerializeField]
    private float knockbackForce; //explosion force of the collision point used for the knockback

	private float windForce;
    private Submarine submarine;
    private CameraShake cameraShake;

    void Start()
    {
        submarine = GetComponent<Submarine>();
        cameraShake = Camera.main.GetComponent<CameraShake>();
    }

    //Submarine reaction to collisions
	void OnCollisionEnter(Collision collision){
        if (!Network.isServer)
            return;
		if(collision.gameObject.layer == LayerMask.NameToLayer("Debris"))
			return;

        submarine.health -= collisionDamage;

		AudioManager.instance.playCollsion();

		//Collision metrics
		Vector3 collisionPos = collision.contacts[0].point;
		//float speedModifier = Mathf.Clamp(submarine.currentSpeed-3.0f, 1.0f, submarine.currentMaxSpeed*0.3f);
		float speedModifier = Mathf.Clamp(submarine.currentSpeed-3.0f, 1.0f, 1.5f);
		Vector3 direction = Vector3.Normalize(collisionPos - transform.position);

		if (collision.gameObject.CompareTag("DestructibleObstacle"))
        {
			//Destroy obstacle on impact
			collision.gameObject.GetComponent<NetworkView>().RPC("impact", RPCMode.All, transform.position);

			//Knockback (weaker) - uses RigidBodyExtensions
			rigidbody.AddExplosionForce (knockbackForce * submarine.submarineMass * speedModifier, transform.position, 0f, direction * (knockbackForce*0.005f));
		}
		else
        {
			//Knockback (stronger)- uses RigidBodyExtensions
			rigidbody.AddExplosionForce (knockbackForce * submarine.submarineMass * speedModifier, transform.position, 0f, direction * (knockbackForce*0.01f));
		}

        networkView.RPC("OnHurtingCollision", RPCMode.All, collision.contacts[0].point.x < transform.position.x ? -1.0f : 1.0f);
	}

	void OnTriggerEnter(Collider collision)
	{
        if (!Network.isServer)
            return;

		if (collision.CompareTag ("WindZone")) {
			submarine.isInWindZone = true;
			windForce = collision.GetComponent<Windforce>().force;
			submarine.windForce = collision.transform.forward * Time.deltaTime * windForce * submarine.submarineMass;
		}
	}

	void OnTriggerExit(Collider collision)
	{
        if (!Network.isServer)
            return;

		if (collision.CompareTag ("WindZone")) {
			submarine.isInWindZone = false;
			submarine.windForce = new Vector3(0.0f, 0.0f, 0.0f);
		}
	}

    [RPC]
    void OnHurtingCollision(float direction)
    {
        if (GameMaster.instance.firstTimeCol == true && GameMaster.instance.gameIsOngoing)
        {
            GameMaster.instance.firstTimeCol = false;
            TutorialUI.instance.repair = true;
        }

        if (direction < 0.0f)
            cameraShake.DoShake(1.0f, 0.0f);
        else
            cameraShake.DoShake(0.0f, 1.0f);
    }
    
}
