﻿using UnityEngine;
using System.Collections;

public class Torpedo : MonoBehaviour {

	//Public Vars
	public float maxAliveTime = 5.0f;

	public GameObject explosionEffect;

	//Private Vars
	private float aliveTime;

    private bool isServer;

    private Vector3 truePosition;
    private Quaternion trueRotation;

	// Use this for initialization
	void Start () {
        isServer = Network.isServer;
        rigidbody.isKinematic = !isServer;

		aliveTime = 0.0f;

        truePosition = transform.position;
        trueRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        if (!isServer)
        {
            transform.position = Vector3.Lerp(transform.position, this.truePosition, Time.deltaTime * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, this.trueRotation, Time.deltaTime * 5);
            return;
        }

		aliveTime = aliveTime + Time.deltaTime;
		//Destroy any torpedo that has been out for too long to release memory.
		if (aliveTime >= maxAliveTime){
            networkView.RPC("Explode", RPCMode.All);
		}
	}

	//Collisions
	void OnCollisionEnter(Collision other){
        if (!isServer) return;

		//Destroy both the torpedo and the obstacle.
		if (other.gameObject.tag == "DestructibleObstacle") {
			other.gameObject.GetComponent<NetworkView>().RPC("impact", RPCMode.All, transform.position);
		}

        networkView.RPC("Explode", RPCMode.All);
	}

    //Position streaming from server to client
    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        if (stream.isWriting)
        {
            truePosition = transform.position;
            stream.Serialize(ref truePosition);

            trueRotation = transform.rotation;
            stream.Serialize(ref trueRotation);
        }
        else
        {
            stream.Serialize(ref truePosition);
            stream.Serialize(ref trueRotation);
        }
    }

	//Generate explosion
    [RPC] void Explode(){
		//Insert effect here
		Network.Instantiate(explosionEffect, transform.position, transform.rotation, 0);
        Destroy(gameObject);
	}
}
