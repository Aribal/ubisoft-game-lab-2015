﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Script that shoots a projectile from the center of the object its attached to.
/// Will apply a force to the parent in the opposite direction of its (the object) forward vector.
/// </summary>

public class Canon : AimableModule {

    // Inspector variables

    [SerializeField]
    private Rigidbody projectile;
    [SerializeField]
    private float speed = 20.0f;
    [SerializeField]
    private float recoilSpeed = 0.12f;
    [SerializeField]
    private float maxtime = 3.0f;
    [SerializeField]
    private float breakForce = 3.0f;

    //Private Variables which arent useful for tweaking.
    private bool isRecoiling;
	private bool isKnockback;
    private float time;
    private Vector3 directionAtShoot;
	private Transform firingPosition;
    private CameraShake cameraShake;

	public bool shootsFlares = false;

	// Use this for initialization
	void Start () {
        isRecoiling = false;
		isKnockback = false;
        time = 0.0f;

		firingPosition = transform.FindChild("FiringPosition");
        cameraShake = transform.parent.GetComponentInChildren<CameraShake>();

        base.Initialize();
	}
	
	// Update is called once per frame
	void Update ()
    {
        base.UpdateModuleRotation();

        if (Network.isClient)
            return;




		//If we are recoiling, move the parent in an opposite direction.
		//Checks for how long we have been recoiling.
       if (isRecoiling == true)
       {
			
			float speed = recoilSpeed * Time.deltaTime;

			transform.parent.Translate(speed * Vector3.Normalize(-directionAtShoot),Space.World);
			
            time = time + Time.deltaTime;

			//If we have recoiled for long enough, we stop.
			if (time >= maxtime) 
			{
				time = 0.0f;
				isRecoiling = false;
			}
        }

	}

    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        base.StreamRotation(stream);
    }

	public override float GetType(){
		if(shootsFlares)
			return 2;
		else
			return 1;
	}

    public override void Use()
    {
        if (isRecoiling)
            return;

		audio.Play();

        if (!Network.isServer)
        {
            cameraShake.DoShake();
            return;
        }

		//Is needed for the recoiling to happen.
		isRecoiling = true;
		isKnockback = true;
        time = 0.0f;
        directionAtShoot = transform.forward;

        //Creates a projectile of which we can define the type when applying the script to an object.
		Rigidbody projectileInstance = Network.Instantiate(projectile, firingPosition.position + 1 * firingPosition.forward, transform.rotation, 0) as Rigidbody;
        Physics.IgnoreCollision(projectileInstance.collider, transform.root.collider);
        projectileInstance.velocity = transform.TransformDirection(new Vector3(0.0f, 0.0f, speed));

        cameraShake.DoShake();
    }

    public override void SendAxis(float horizontal, float vertical)
    {
        base.RotateCamera(horizontal, vertical);
    }

}
