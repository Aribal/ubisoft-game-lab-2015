﻿using UnityEngine;
using System.Collections;

public class Propeller : Module {

	[SerializeField] private Transform TopPropeller;
	[SerializeField] private Transform BottomPropeller;
	[SerializeField] private Transform LeftPropeller;
	[SerializeField] private Transform RightPropeller;
	[SerializeField] private Transform BackPropeller;

	[SerializeField] private float backPropellerRotationSpeed;
	[SerializeField] private float backPropellerAcceleration;
	[SerializeField] private float rotationSpeed;

    private Submarine _submarine;
    private float _currentPropellerRotationSpeed;
    private MotionBlur _motionBlur;

	void Start () {
        _submarine = GetComponent<Submarine>();
        _currentPropellerRotationSpeed = backPropellerRotationSpeed;
        _motionBlur = Camera.main.GetComponent<MotionBlur>();
        _motionBlur.enabled = false;
	}

	void Update() {
		/*animates working propellers and add particle effects for those working*/
		if (_submarine.moveVertical > 0.0f) {
			if(!_submarine.upParticles.isPlaying)
				_submarine.upParticles.Play ();
			TopPropeller.Rotate (Vector3.forward, rotationSpeed * Time.deltaTime, Space.Self);
		} else if (_submarine.moveVertical < 0.0f) {
			BottomPropeller.Rotate (Vector3.forward, rotationSpeed * Time.deltaTime, Space.Self);
			if(!_submarine.bottomParticles.isPlaying)
				_submarine.bottomParticles.Play ();
		}
		if (_submarine.moveHorizontal > 0.0f) {
			LeftPropeller.Rotate (Vector3.forward, rotationSpeed * Time.deltaTime, Space.Self);
			if(!_submarine.leftParticles.isPlaying)
				_submarine.leftParticles.Play ();
		} else if (_submarine.moveHorizontal < 0.0f) {
			RightPropeller.Rotate (Vector3.forward, rotationSpeed * Time.deltaTime, Space.Self);
			if(!_submarine.rightParticles.isPlaying)
				_submarine.rightParticles.Play ();
		}

		if (_submarine.moveHorizontal == 0.0f) {
			_submarine.leftParticles.Stop();

			_submarine.rightParticles.Stop ();
		}

		if (_submarine.moveVertical == 0.0f) {
			_submarine.upParticles.Stop ();
			_submarine.bottomParticles.Stop ();
		}

        BackPropeller.Rotate(Vector3.forward, _currentPropellerRotationSpeed * Time.deltaTime, Space.Self);
	}

	public override float GetType(){
		return 0;
	}

    public override void SendAxis(float horizontal, float vertical)
    {
		vertical *= -1.0f;
        if (Network.isServer)
        {
            //_submarine.MoveHorizontal(horizontal);
			networkView.RPC("TransmitHorizontalMovement", RPCMode.All, horizontal);

            if (ApplicationManager.instance.isTestingSolo)
                _submarine.MoveVertical(vertical);
        }
        else
        {
			networkView.RPC("TransmitVerticalMovement", RPCMode.All, vertical);
        }
    }

    public override void Use()
    {
        _submarine.Accelerate();
        _currentPropellerRotationSpeed += backPropellerAcceleration;
        if (_currentPropellerRotationSpeed > backPropellerRotationSpeed + 2.0f * backPropellerAcceleration)
            _currentPropellerRotationSpeed = backPropellerRotationSpeed + 2.0f * backPropellerAcceleration;
        //_motionBlur.enabled = true;
    }

    public override void StopUse()
    {
        _submarine.Decelerate();
        _currentPropellerRotationSpeed -= backPropellerAcceleration;
        if (_currentPropellerRotationSpeed <= backPropellerRotationSpeed)
        {
            _currentPropellerRotationSpeed = backPropellerRotationSpeed;
            //_motionBlur.enabled = false;
        }
    }


    [RPC] private void TransmitVerticalMovement (float input)
    {
        _submarine.MoveVertical(input);
    }

	[RPC] private void TransmitHorizontalMovement (float input)
	{
		_submarine.MoveHorizontal(input);
	}

}
