﻿using UnityEngine;
using System.Collections;

public abstract class AimableModule : Module {

    private const float IMAGINARY_POINT_DISTANCE = 5000.0f; // if there is not hit, we still use an imaginary distant point for targeting

    // Inspector variable
    [SerializeField]
    protected CameraAim _cameraController;
    [SerializeField]
    protected LayerMask _raycastLayerMask;
    [SerializeField]
    protected bool _isPlayerOneModule;

    // Protected variables

    protected Vector3 targetPosition;
    protected Vector3 truePosition;
    protected Quaternion trueRotation;
    protected Transform cameraTransform;
    protected Quaternion initialRotation;
    protected bool isControlled;
    protected bool isMine;

    public override void OnSelected()
    {
        if (isMine)
        {
            InGameUIManager.instance.ToggleCrosshair(true);
            _cameraController.isCameraControlled = true;
        }
        isControlled = true;
        targetPosition = transform.position + transform.forward;
    }

    public override void OnUnselected()
    {
        base.OnUnselected();
        if (isMine)
        {
            InGameUIManager.instance.ToggleCrosshair(false);
            _cameraController.isCameraControlled = false;  
        }
        isControlled = false;
    }

    protected void Initialize()
    {
        cameraTransform = _cameraController.transform;
        initialRotation = transform.localRotation;
        isControlled = false;
        isMine = (Network.isServer && _isPlayerOneModule) || (Network.isClient && !_isPlayerOneModule);
    }

    protected void RotateCamera(float horizontal, float vertical)
    {
        if (isMine)
        {
            _cameraController.RotateHorizontal(horizontal);
            _cameraController.RotateVertical(vertical);
        }

        if (Network.isServer && isMine)
        {
            RaycastHit hit;
            if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, Mathf.Infinity, _raycastLayerMask.value))
                targetPosition = hit.point;
            else
                targetPosition = cameraTransform.position + IMAGINARY_POINT_DISTANCE * cameraTransform.forward;

            //Debug.DrawRay(cameraTransform.position, cameraTransform.forward * (hit.point - cameraTransform.position).magnitude, Color.red);
            //Debug.Log(hit.collider.name);
        }
        else if (Network.isClient)
        {
            networkView.RPC("TransmitCameraForward", RPCMode.Server, _cameraController.transform.forward);
        }
    }

    protected void UpdateModuleRotation()
    {
        if (Network.isClient)
        {
            //Client corrects its rotation
            transform.rotation = Quaternion.Lerp(transform.rotation, trueRotation, 5.0f);
            return;
        }
        
        //Server rotates the canon

        if (isControlled)
        {
            //Rotation looking at target
            Quaternion lookRotation = Quaternion.LookRotation(targetPosition - transform.position);

            //Rotate towards target
			transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, 20.0f);
        }
        else
        {
			transform.localRotation = Quaternion.Slerp(transform.localRotation, initialRotation, Time.deltaTime*3.0f);
        }
        
    }

    //Rotation streaming from server to client
    protected void StreamRotation(BitStream stream)
    {
        if (stream.isWriting)
        {
            //Serialize submarine rotation
            trueRotation = transform.rotation;
            stream.Serialize(ref trueRotation);
        }
        else
        {
            //Serialize submarine rotation
            stream.Serialize(ref trueRotation);
        }
    }

    [RPC]
    protected void TransmitCameraForward(Vector3 fwd)
    {
        RaycastHit hit;
        if (Physics.Raycast(cameraTransform.position, fwd, out hit, Mathf.Infinity, _raycastLayerMask.value))
            targetPosition = hit.point;
        else
            targetPosition = cameraTransform.position + IMAGINARY_POINT_DISTANCE * fwd;
    }

}
