﻿using UnityEngine;
using System.Collections;

public class CameraAim : MonoBehaviour {

    // Inspector variables

    [SerializeField]
    private float sensitivityX = 10.0f;
    [SerializeField]
    private float sensitivityY = 10.0f;
	[SerializeField]
	private float maxHorizontalAimAngle = 90;

    public bool isCameraControlled
    {
        get { return _isCameraControlled; }
        set
        {
            _isCameraControlled = value;
            _givenAngle = 0.0f;
        }
    }

    private Quaternion _initialRotation;
    private float _givenAngle = 0.0f;
    private bool _isCameraControlled = false;

	void Start()
    {
		_initialRotation = transform.localRotation;
        _isCameraControlled = false;
	}

	void Update()
    {
        if (!_isCameraControlled)
            transform.localRotation = Quaternion.Slerp(transform.localRotation, _initialRotation, Time.deltaTime * 3.0f);
	}

    public void RotateHorizontal(float val)
    {
        float rotationAngle = val * sensitivityX * Time.deltaTime;
        _givenAngle += rotationAngle;

        if (maxHorizontalAimAngle != 0.0f)
        {
            if (_givenAngle >= maxHorizontalAimAngle)
            {
                _givenAngle = maxHorizontalAimAngle;
                return;
            }
            else if (_givenAngle <= -maxHorizontalAimAngle)
            {
                _givenAngle = -maxHorizontalAimAngle;
                return;
            }
        }

        transform.Rotate(Vector3.up, rotationAngle, Space.World);

		/*//Lock X and Y axis
		Vector3 clampedRotation = transform.localRotation.eulerAngles;
		
		//Lock X
		if(360-clampedRotation.x > 180){
			clampedRotation.x = Mathf.Clamp(clampedRotation.x, 0, maxAimAngle);
		}
		else if(360-clampedRotation.x < 180){
			clampedRotation.x = Mathf.Clamp(clampedRotation.x, 360+maxAimAngle, 360);
		}
		//Lock Y
		clampedRotation.y = Mathf.Clamp(clampedRotation.y, -maxAimAngle, maxAimAngle);
		//Lock Z
		clampedRotation.z = Mathf.Clamp(clampedRotation.z, 0, 0);
		
		//Apply lock
		transform.localRotation = Quaternion.Euler(clampedRotation);*/
	}

    public void RotateVertical(float val)
    {
        transform.Rotate(Vector3.right, val * sensitivityY * Time.deltaTime, Space.Self);
    }

}
