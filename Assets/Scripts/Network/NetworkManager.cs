﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contain static methods for Unity's network connections.
/// </summary>
public class NetworkManager : MonoBehaviour {

    private const string GAMETYPE_NAME = "SubmarineUQAC";
    private const int PORT = 25000;

    /// <summary>
    /// Initialize a game server and register it to Unity's Master Server.
    /// </summary>
    /// <param name="gameName">Name of the server.</param>
    /// <param name="description">Description of the server</param>
    public static void HostServer(string gameName = "", string description = "")
    {
        if (string.IsNullOrEmpty(gameName))
            gameName = SystemInfo.deviceName;

        Network.InitializeServer(1, PORT, !Network.HavePublicAddress());

        MasterServer.RegisterHost(GAMETYPE_NAME, gameName, description);
    }

    public static void HostLocalServer()
    {
        Network.InitializeServer(1, PORT, !Network.HavePublicAddress());
    }

    /// <summary>
    /// Join a game server.
    /// </summary>
    /// <param name="ipAdress">IP adress of the server.</param>
    public static void JoinServer(string ipAdress)
    {
        Network.Connect(ipAdress, PORT);
    }

    /// <summary>
    /// Join a game server.
    /// </summary>
    /// <param name="hostData">Host data of the server.</param>
    public static void JoinServer(HostData hostData)
    {
        Network.Connect(hostData);
    }

    /// <summary>
    /// Request the current server list of the game from Unity's Master Server.
    /// </summary>
    public static void RequestServerList()
    {
        MasterServer.ClearHostList();
        MasterServer.RequestHostList(GAMETYPE_NAME);
    }

    /// <summary>
    /// Disconnect from server if client, shut it down if server.
    /// </summary>
    public static void Disconnect()
    {
        Network.Disconnect();
    }
}
